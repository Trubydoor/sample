use rand::prelude::*;
use rayon::prelude::*;

use structopt::StructOpt;

#[derive(StructOpt)]
struct Args {
    #[structopt(short = "i", long = "iterations", default_value = "200000")]
    iterations: i32,
    #[structopt(short = "r", long = "random", default_value = "20")]
    random: i32,
    #[structopt(short = "p", long = "parallel")]
    parallel: bool
}

fn serial(iterations: i32, random_val: i32) {
    let mut cumulative_positives = 0;
    for k in 1..iterations+1 {
        let mut batch_positives = 0;
        let mut batch_negatives = 0;

        for _ in 1..101 {
            let range = rand::distributions::Uniform::from(1..random_val+1);
            let mut samples =
              rand::thread_rng().sample_iter(&range).take(10);

            if samples.any(|x| x == 1) {
                batch_positives += 1;
            } else {
                batch_negatives += 1;
            }
        }

        cumulative_positives += batch_positives;
        let average_positives = cumulative_positives as f32 / k as f32;
        println!("Iteration {} Batches with Positives {} Batches with Negatives {} Cumulative Positives {} Running average {}",
                 k, batch_positives, batch_negatives, cumulative_positives, average_positives);
    };
}

fn parallel(iterations: i32, random_val: i32) {
    let cumulative_positives : i32 = (1..iterations+1).into_par_iter().map(|_| {
        let mut batch_positives = 0;
        let mut batch_negatives = 0;

        (1..101).for_each(|_| {
            let range = rand::distributions::Uniform::from(1..random_val+1);
            let mut samples =
              rand::thread_rng().sample_iter(&range).take(10);

            if samples.any(|x| x == 1) {
                batch_positives += 1;
            } else {
                batch_negatives += 1;
            }
        });

        return batch_positives;
    }).sum();
    let average_positives = cumulative_positives as f64 / iterations as f64;
    println!("Average {} after {} iterations", average_positives, iterations);
}

fn main() {
    let args = Args::from_args();
    if args.parallel {
        parallel(args.iterations, args.random);
    } else {
        serial(args.iterations,args.random);
    }
}
